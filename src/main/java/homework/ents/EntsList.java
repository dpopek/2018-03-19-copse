package homework.ents;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.ChoiceBoxTableCell;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.layout.Region;
import javafx.stage.FileChooser;
import javafx.util.converter.IntegerStringConverter;

import java.io.File;
import java.net.URL;
import java.util.ResourceBundle;

public class EntsList implements Initializable {

    private ResourceBundle rb;
    private ObservableList<Ent> ents = FXCollections.observableArrayList();

    @FXML
    private TableView entsTable;

    @FXML
    private TableColumn<Ent, Integer> plantYearColumn;

    @FXML
    private TableColumn<Ent, Integer> heightColumn;

    @FXML
    private TableColumn<Ent, String> speciesColumn;

    @FXML
    private TableColumn<Ent, Ent.Type> typeColumn;

    @FXML
    private ProgressBar progerss;

    @Override
    public void initialize(URL location, ResourceBundle resourceBundle) {

        entsTable.setItems(ents);
        rb = resourceBundle;

        ents.add(new Ent(1823, 303, "Celtis occidentalis", Ent.Type.coniferous));
        ents.add(new Ent(1975, 345, "Betula papyrifera", Ent.Type.decidous));
        ents.add(new Ent(1999, 293, "Carya illinoinensis", Ent.Type.decidous));
        ents.add(new Ent(2005, 105, "Carpinus caroliniana", Ent.Type.coniferous));

        progerss.setProgress(0);

        plantYearColumn.setCellFactory(TextFieldTableCell.forTableColumn(new IntegerStringConverter()));
        plantYearColumn.setOnEditCommit(event -> event.getRowValue().setPlantYear(event.getNewValue()));

        heightColumn.setCellFactory(TextFieldTableCell.forTableColumn(new IntegerStringConverter()));
        heightColumn.setOnEditCommit(event -> event.getRowValue().setHeight(event.getNewValue()));

        typeColumn.setCellFactory(ChoiceBoxTableCell.forTableColumn(Ent.Type.values()));
        typeColumn.setOnEditCommit(event -> event.getRowValue().setType(event.getNewValue()));

        speciesColumn.setCellFactory(ChoiceBoxTableCell.forTableColumn(Ent.speciesList));
        speciesColumn.setOnEditCommit(event -> event.getRowValue().setSpecies(event.getNewValue()));
    }

    @FXML
    private void aboutPopup(ActionEvent event) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle(rb.getString("aboutTitle"));
        alert.setHeaderText(rb.getString("aboutHeader"));
        alert.setContentText(rb.getString("aboutContent"));
        alert.getDialogPane().setMinHeight(Region.USE_PREF_SIZE);
        alert.showAndWait();
    }

    @FXML
    private void open(ActionEvent event) {
        FileChooser chooser = new FileChooser();
        File file = chooser.showOpenDialog(((MenuItem) event.getTarget()).getParentPopup().getScene().getWindow());
        if (file != null) {
            open(file);
        }
    }

    private void open(File file) {
        OpenWorker worker = new OpenWorker(ents, file);
        progerss.progressProperty().bind(worker.progressProperty());
        new Thread(worker).start();
    }

    @FXML
    private void save(ActionEvent event) {
        FileChooser chooser = new FileChooser();
        File file = chooser.showSaveDialog(((MenuItem) event.getTarget()).getParentPopup().getScene().getWindow());
        if (file != null) {
            save(file);
        }
    }

    private void save(File file) {
        SaveWorker worker = new SaveWorker(ents, file);
        progerss.progressProperty().bind(worker.progressProperty());
        new Thread(worker).start();
    }

    @FXML
    private void plant(ActionEvent event) {
        ents.add(new Ent());
    }

    @FXML
    private void cut(ActionEvent event) {
        if (entsTable.getSelectionModel().getSelectedIndex() >= 0) {
            ents.remove(entsTable.getSelectionModel().getSelectedIndex());
            entsTable.getSelectionModel().clearSelection();
        }

    }

    @FXML
    private void rootTheSeeding(ActionEvent event) {
        if (entsTable.getSelectionModel().getSelectedIndex() >= 0) {
            Ent root = new Ent();
            root = ents.get(entsTable.getSelectionModel().getSelectedIndex());
            ents.add(new Ent(2018, 10, root.getSpecies(), root.getType()));
        }
    }

}