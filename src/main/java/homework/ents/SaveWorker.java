package homework.ents;

import javafx.concurrent.Task;
import java.io.*;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SaveWorker extends Task<Void> {
    private static final Logger log = Logger.getLogger(SaveWorker.class.getCanonicalName());

    private Collection<Ent> ents;
    private File file;

    public SaveWorker(Collection<Ent> ents, File file) {
        this.ents = ents;
        this.file = file;
    }

    @Override
    protected Void call() throws Exception {
        int i = 0;
        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(file))) {
            oos.writeObject(ents.size());
            for (Ent ent : ents){
                oos.writeObject(ent);
                ++i;
                updateProgress(i, ents.size());
            }
        }catch (IOException ex){
            log.log(Level.WARNING, ex.getMessage(), ex);
        }
        return null;
    }
}
