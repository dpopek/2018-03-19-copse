package homework.ents;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.net.URL;
import java.util.ResourceBundle;

public class CopseMain extends Application {

    public static void main(String[] args){
        launch();
    }

    @Override
    public void start(Stage stage) throws Exception {
        URL fxml = this.getClass().getResource("/homework/ents/ents_list.fxml");
        ResourceBundle rb = ResourceBundle.getBundle("homework.ents.messages.ents_list_msg");

        Parent root = FXMLLoader.load(fxml, rb);

        Scene scene = new Scene(root);
        stage.setTitle("Ents manager");
        scene.getStylesheets().add("/homework/ents/css/ents_list.css");

        stage.setScene(scene);
        stage.show();
    }
}
